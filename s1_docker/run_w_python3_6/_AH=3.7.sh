#!/bin/bash
SH=`cd $(dirname $BASH_SOURCE) && pwd`
GH=`cd "$SH/../.." && pwd`  # GH aka GIT_CLONED_HOME
AH="$GH/s0_source_code/3.7--dict-key-ordering-preserved" \
c='myapp_py3_6' \
    source "$SH/_.sh"

note=cat<<EOT
Look closely at True line in below console output where dict key in :d IS preserved as declared-ordering in :o
EOT

sample_console_output=cat<<EOT
myapp_py3_6
myapp_py3_6

from myapp_py3_6

Python 3.6.15

-rw-rw-r-- 1 1001 1001 582 Oct 14 12:28 /app/app.py

fr code written for python 3.7+

o=['a', 'bb', 'C', 'D']
{'a': 1, 'bb': 22, 'C': 333, 'D': 4444}
['a', 'bb', 'C', 'D']
True
{'a': 1, 'bb': 22, 'C': 333, 'D': 4444}
['a', 'bb', 'C', 'D']
True

o=[3, 1, 2]
{3: 3, 1: 1, 2: 2}
[3, 1, 2]
True
{'3': 3, '1': 1, '2': 2}
['3', '1', '2']
False

EOT
