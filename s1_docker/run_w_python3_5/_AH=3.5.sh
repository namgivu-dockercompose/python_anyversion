#!/bin/bash
SH=`cd $(dirname $BASH_SOURCE) && pwd`
GH=`cd "$SH/../.." && pwd`  # GH aka GIT_CLONED_HOME

AH="$GH/s0_source_code/3.5" \
c='myapp_py3_5' \
    source "$SH/_.sh"
