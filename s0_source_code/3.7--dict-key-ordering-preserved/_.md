ref. https://stackoverflow.com/questions/31715344/how-to-preserve-ordering-of-items-in-a-python-dictionary#comment95914786_31715344
> dict order preserving in Python 3.6 as an implementation detail, and as a language feature in 3.7 

ref. https://docs.python.org/release/3.7.0/whatsnew/changelog.html#id36
> bpo-32697: Python now explicitly preserves the definition order of keyword-only parameters. It’s always preserved their order, but this behavior was never guaranteed before; this behavior is now guaranteed and tested.

ref. https://gandenberger.org/2018/03/10/ordered-dicts-vs-ordereddict/
> this order-preserving property is becoming a language feature in Python 3.7.
> You might think that this change makes the OrderedDict class obsolete.
