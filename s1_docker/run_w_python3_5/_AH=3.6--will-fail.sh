#!/bin/bash
SH=`cd $(dirname $BASH_SOURCE) && pwd`
GH=`cd "$SH/../.." && pwd`  # GH aka GIT_CLONED_HOME
AH="$GH/s0_source_code/3.6" \
c='myapp_py3_5' \
    source "$SH/_.sh"

explain=cat<<EOT
container FROM python:3.5 will not have f-string as python:3.6+ --> running 3.6 app.py will raise syntax error
eg error
    File "/app/app.py", line 4
        name = 'Nam' ; print(f'f-string hi {name}')
                                                 ^
    SyntaxError: invalid syntax

EOT