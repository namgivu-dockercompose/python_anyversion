#!/bin/bash
[ -z $AH ]            && (echo 'Envvar AH is required'; kill $$)
[ ! -f "$AH/app.py" ] && (echo 'app.py is required'; kill $$)

[ -z $c ]             && (echo 'Envvar c for container_name is required'; kill $$)

SH=`cd $(dirname $BASH_SOURCE) && pwd`
cd $SH

docker stop $c && docker rm $_ ; docker run \
    --name $c -h$c               `# container name n hostname` \
    -v "$AH/app.py:/app/app.py"  `# file mapping @ set app.py code` \
    `docker build -q .`          `# dockerimage buildonthefly ref. https://stackoverflow.com/a/51314059/248616`
