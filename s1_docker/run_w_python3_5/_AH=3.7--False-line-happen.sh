#!/bin/bash
SH=`cd $(dirname $BASH_SOURCE) && pwd`
GH=`cd "$SH/../.." && pwd`  # GH aka GIT_CLONED_HOME
AH="$GH/s0_source_code/3.7--dict-key-ordering-preserved" \
c='myapp_py3_5' \
    source "$SH/_.sh"

note=cat<<EOT
Look closely at False line in below console output where dict key in :d not preserved as declared-ordering in :o
EOT

sample_console_output=cat<<EOT
myapp_py3_5
myapp_py3_5

from myapp_py3_5

Python 3.5.10

-rw-rw-r-- 1 1001 1001 582 Oct 14 12:28 /app/app.py

fr code written for python 3.7+

o=['a', 'bb', 'C', 'D']
{'bb': 22, 'C': 333, 'D': 4444, 'a': 1}
['bb', 'C', 'D', 'a']
False
{'bb': 22, 'C': 333, 'D': 4444, 'a': 1}
['bb', 'C', 'D', 'a']
False

o=[3, 1, 2]
{1: 1, 2: 2, 3: 3}
[1, 2, 3]
False
{'3': 3, '1': 1, '2': 2}
['3', '1', '2']
False

EOT
