#!/bin/bash
SH=`cd $(dirname $BASH_SOURCE) && pwd`
AH=$SH
cd $AH
    PYTHONPATH=$AH \
    python3 -m pipenv run \
        python3 "$AH/app.py"
