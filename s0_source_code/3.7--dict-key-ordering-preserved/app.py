print('fr code written for python 3.7+')

def run(d, o):
    print()
    print('o='+str(o))

    print(d) ; l=list(d.keys()) ; print(l) ; print(l == o)

    # scenario fr api endpoint resp's body as str json loaded to python dict  #NOTE json will auto-convert int key to str
    d2 = {**d} ;  import json ; j=json.dumps(d2) ; d2=json.loads(j)
    print(d2) ; l=list(d2.keys()) ; print(l) ; print(l == o)


run(
    d = {'a':1, 'bb':22, 'C':333, 'D':4444},
    o = ['a',   'bb',    'C',     'D', ],  # d's order
)

run(
    d = {3:3, 1:1, 2:2},
    o = [3,   1,   2],  # d's order
)
